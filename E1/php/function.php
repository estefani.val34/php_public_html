<?php

/**
 * calculate +,max, min, avg
 * @param type $checkboxArray
 * @param type $operand1
 * @param type $operand2
 * @return type
 */
function calculate($checkboxArray, $operand1, $operand2) {
    foreach ($checkboxArray as $key => $value) {
        if ($key === sum) {
            $checkboxArray[sum] = $operand1 + $operand2;
        }
        if ($key === max) {
            $checkboxArray[max] = max($operand1, $operand2);
        }
        if ($key === min) {
            $checkboxArray[min] = min($operand1, $operand2);
        }
        if ($key === avg) {
            $checkboxArray[avg] = avg($operand1, $operand2);
        }
    }
    return $checkboxArray;
}

/**
 * calculate average
 * @param type $operand1
 * @param type $operand2
 * @return type
 */
function avg($operand1, $operand2) {
    return ($operand1 + $operand2) / 2;
}

/**
 * show table of operands 
 * @param type $new_arr
 * @param type $operand1
 * @param type $operand2
 */
function table($new_arr, $operand1, $operand2) {
    echo "
        <fieldset>
            <legend>Result</legend>
            
            <table border='1'>
                <caption>Operations</caption>
                <tr>
                    <th>  </th>
                    <th>operand 1</th>
                    <th>operand 2</th>
                    <th>result</th>
                </tr>";
    foreach ($new_arr as $key => $value) {
        echo" <tr>
                        <td>$key</td>
                        <td>$operand1</td>
                        <td>$operand2</td>
                        <td>$value</td>
                    </tr>";
    }

    echo "      </table>
        </fieldset>
";
}
/**
 * validate array and operands
 * @param type $checkboxArray
 * @param type $operand1
 * @param type $operand2
 * @return boolean
 */
function valid_input($checkboxArray, $operand1, $operand2) {
    if (!empty($operand1) && !empty($operand2) && count($checkboxArray) != 0) {
        if (filter_var($operand1, FILTER_VALIDATE_FLOAT) && filter_var($operand2, FILTER_VALIDATE_FLOAT)) {
            return true;
        }
    }
    return false;
}

/**
 * Show an error table
 * @param type $errors
 */
function print_errors($errors) {
    echo <<<EOT
        <fieldset>
            <legend>Errors</legend>
            
            <table border='1'>
                <caption>Validation errors</caption>
                <tr>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>$errors</td>
                </tr>
            </table>
        </fieldset>
EOT;
}
