<?php
require_once 'php/function.php';

define(suma, "sum");
define(maximo, "max");
define(minimo, "min");
define(avg, "avg");

$operand1 = NULL;
$operand2 = NULL;

$checkedBoxSum = NULL;
$checkedBoxMax = NULL;
$checkedBoxMin = NULL;
$checkedBoxAvg = NULL;

$checkboxArray = NULL;

if ((filter_has_var(INPUT_POST, 'n_submit'))) {

    $operand1 = trim(filter_input(INPUT_POST, 'n_operand1'));
    $operand2 = trim(filter_input(INPUT_POST, 'n_operand2'));


    $checkboxArray = filter_input(INPUT_POST, 'n_checkbox', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    if (isset($checkboxArray)) {
        foreach ($checkboxArray as $checkboxValue) {
            switch ($checkboxValue) {
                case suma:
                    $checkedBoxSum = "checked";
                    break;
                case maximo:
                    $checkedBoxMax = "checked";
                    break;
                case minimo:
                    $checkedBoxMin = "checked";
                    break;
                case avg:
                    $checkedBoxAvg = "checked";
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post" enctype="multipart/form-data">   
            <fieldset>
                </br>
                <label for="id_operand1">Input operand 1:</label>
                <input type="text" id="id_operand1" name="n_operand1" value="<?= $operand1 ?>"/>

                <label for="id_operand2">Input operand 2:</label>
                <input type="text" id="id_operand2" name="n_operand2" value="<?= $operand2 ?>"/>
                <!--checkbox-->
                <input type="checkbox" id="id_checkbox1" name="n_checkbox[]" value="sum" <?= $checkedBoxSum ?>/>sum
                <input type="checkbox" id="id_checkbox2" name="n_checkbox[]" value="max" <?= $checkedBoxMax ?>/>max
                <input type="checkbox" id="id_checkbox3" name="n_checkbox[]" value="min" <?= $checkedBoxMin ?>/>min
                <input type="checkbox" id="id_checkbox4" name="n_checkbox[]" value="avg" <?= $checkedBoxAvg ?>/>avg
                </br>
                </br>
                </br>

                <input type="submit" id="id_submit1" name="n_submit" value="calc" />

            </fieldset>
        </form>
        <?php
        echo "</br>";
        echo "</br>";
        echo '<link href="css/style.css" type="text/css" rel="stylesheet">';

        $checkboxArray = array_flip($checkboxArray);
        $new_arr = NULL;


        if ((filter_has_var(INPUT_POST, 'n_submit'))) {

            if (valid_input($checkboxArray, $operand1, $operand2)) {
                $new_arr = calculate($checkboxArray, $operand1, $operand2);
                echo '</br>';
                table($new_arr, $operand1, $operand2);
            } else {
                $errors = "You have not introduced a correct operand";
                print_errors($errors);
            }
        }
        ?>
    </body>
</html>
