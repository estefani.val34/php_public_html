<?php

require_once 'SequenceInterface.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeneticsSequence
 *
 * @author tarda
 */
abstract class GeneticsSequence implements SequenceInterface {

    const BASE_T = "T";
    const BASE_U = "U";

    private $id;
    private $elements; //cadena de string
    private $validValues; // lo valido con los string 

    function __construct($id, $elements, $validValues) {
        $this->id = $id;
        $this->elements = $elements;
        $this->validValues = $validValues;
    }

    function getId() {
        return $this->id;
    }

    function getElements() {
        return $this->elements;
    }

    function getValidValues() {
        return $this->validValues;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setElements($elements) {
        $this->elements = $elements;
    }

    function setValidValues($validValues) {
        $this->validValues = $validValues;
    }

    public function __toString() {
        return  sprintf("GeneticsSequence: {id=%d }", $this->id);
        
    }

    public function validate($elements,$validValues) { // VALIDAR LAS 
        
        for ($i = 0; $i < count($elements); $i++) {
            for ($index = 0; $index < count($validValues); $index++) {
                if ($elements[i]==$validValues[index]) {
                    return true;
                }
            }
        }
        return false;
    }

    public function countBases() {//CONTAR LAS LETRAS
    }

    public abstract function transcription(); //PASAR DE ARN A ADN
}
