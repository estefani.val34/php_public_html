<?php
require_once 'GeneticsSequence.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DNASequence
 *
 * @author tarda
 */
class DNASequence extends GeneticsSequence{
  
    const VALID_VALUES='ACGT';
    const TYPE='DNA';
    
    function __construct($id, $elements) {
        parent::__construct($id, $elements, self::VALID_VALUES);
    }

    public function transcription() {
        return "";
    }
    public function __toString() {
       return sprintf( "%s; validValues=%s; type=%s",parent::__toString(), self::VALID_VALUES, self::TYPE);
    }

}
