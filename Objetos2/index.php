<?php
require_once 'clasesitf/GeneticsSequence.php';
require_once 'clasesitf/RNASequence.php';
require_once 'clasesitf/DNASequence.php';

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $RNA1 = new RNASequence(1, "AGCUUAA");
        $RNA2 = new DNASequence(2, "ATTGAT");
        echo "<p>" . $RNA1 . " </p>";
        echo "<p>" . $RNA2 . "</p>";
       
        ?>
    </body>
</html>
