<?php

require_once "controller/ControllerInterface.php";
require_once "view/ProductView.class.php";
//require_once "model/ProductModel.class.php";
require_once "model/Product.class.php";
//require_once "util/ProductMessage.class.php";
//require_once "util/ProductFormValidation.class.php";

class ProductController implements ControllerInterface {

    private $view;
    private $model;

    function __construct() {
        $this->view = new ProductView();
       // $this->model = new ProductModel();
    }

// carga la vista según la opción o ejecuta una acción específica
    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        // recupera la acción de un formulario
        if (filter_has_var(INPUT_POST, 'action')) {
            $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;
        }
        // recupera la opción de un menú
        else {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {
            case "form_add":
                $this->formAdd();
                break;
            case "add":
                $this->add();
                break;
            case "list_all":
                $this->listAll();
                break;
            default:
                $this->view->display();
        }
    }

    public function add() {
        
    }

    public function delete() {
        
    }

    public function listAll() {
        /*$products=$this->model->listAll();
        
        if (!empty($products)) { // array void or array of Category objects?
            $_SESSION['info']=CategoryMessage::INF_FORM['found'];
        }
        else {
            $_SESSION['error']=CategoryMessage::ERR_FORM['not_found'];
        }
        */
        $products=array();
        $this->view->display("view/form/ProductList.php", $products);
        
        
        
    }

    public function modify() {
        
    }

    
  

    public function searchById() {
        
        
        
    }

}
