<?php
    function valid_input($cels, $fahren):bool {
        if (!is_numeric($cels) || !is_numeric($fahren)) {
            return false;
        }
        return true;
    }

    function fahren_to_cels(int $fahren):float {
        $cels=round(($fahren-32)/1.8, 2);
        return $cels;
    }

    function cels_to_fahren(int $cels):float {
        $fahren=round(($cels*1.8)+32, 2);
        return $fahren;
    }

    function print_temp($cels, $fahren, $cels_result, $fahren_result) {  
echo <<<EOT
        <fieldset>
            <legend>Result</legend>

            <table border='1'>
                <caption>Celsius to Fahrenheit</caption>
                <tr>
                    <th>Celsius</th>
                    <th>Fahrenheit</th>
                </tr>
                <tr>
                    <td>$cels</td>
                    <td>$fahren_result</td>
                </tr>
            </table>
        
            <table border='1'>
                <caption>Fahrenheit to Celsius</caption>
                <tr>
                    <th>Fahrenheit</th>
                    <th>Celsius</th>
                </tr>
                <tr>
                    <td>$fahren</td>
                    <td>$cels_result</td>
                </tr>
            </table>
        </fieldset>
EOT;
    }
